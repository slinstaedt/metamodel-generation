package de.kamalook.test.metamodel.generation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.AnnotatedType;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import de.kamalook.test.metamodel.generation.model.MyEntity;
import de.kamalook.test.metamodel.generation.model.OwnerID;

public class OwningContextResolverTest {

  @Test
  void reflection() {
    Optional<AnnotatedType> result =
        OwnedContextResolver.resolveOwnedType(MyEntity.class.getAnnotatedInterfaces());

    assertTrue(result.isPresent());
    assertEquals(OwnerID.class, result.get().getType());
  }
}

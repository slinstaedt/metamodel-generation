package de.kamalook.test.metamodel.generation.model;

import static java.util.Objects.requireNonNull;

import java.time.LocalDate;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import de.kamalook.test.metamodel.generation.Owned;

@Entity
public class MyEntity implements Owned<OwnerID> {

  public enum Sex {
    FEMALE,
    MALE;
  }

  public static MyEntity random() {
    return random(UUID.randomUUID().toString());
  }

  public static MyEntity random(String name) {
    MyEntity entity = new MyEntity();
    entity.name = requireNonNull(name);
    entity.sex = Sex.values()[ThreadLocalRandom.current().nextInt(2)];
    entity.owner = new OwnerID(name);
    entity.birth = LocalDate.ofYearDay(1980, ThreadLocalRandom.current().nextInt(365) + 1);
    return entity;
  }

  @Id @GeneratedValue private UUID id;

  @Embedded private OwnerID owner;

  private String name;

  private LocalDate birth;

  @Enumerated(EnumType.STRING)
  private Sex sex;

  public LocalDate getBirth() {
    return birth;
  }

  public String getName() {
    return name;
  }

  @Override
  public OwnerID getOwner() {
    return owner;
  }

  public Sex getSex() {
    return sex;
  }

  @Override
  public String toString() {
    return "MyEntity [id="
        + id
        + ", owner="
        + owner
        + ", name="
        + name
        + ", birth="
        + birth
        + ", sex="
        + sex
        + "]";
  }
}

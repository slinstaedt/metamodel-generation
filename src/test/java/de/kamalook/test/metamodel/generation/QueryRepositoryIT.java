package de.kamalook.test.metamodel.generation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import de.kamalook.test.metamodel.generation.model.MyEntity;
import de.kamalook.test.metamodel.generation.model.OwnerID;
import de.kamalook.test.metamodel.generation.model.QMyEntity;

public class QueryRepositoryIT {

  private static final String OWNER = "sven";

  public static void main(String[] args) {
    QueryRepositoryIT test = new QueryRepositoryIT();
    test.init();
    test.test();
    test.close();
  }

  private EntityManagerFactory factory;

  private QueryRepository<JPQLQuery<?>> repository;

  @AfterEach
  void close() {
    factory.close();
  }

  @BeforeEach
  void init() {
    factory = Persistence.createEntityManagerFactory("default");
    EntityManager manager = factory.createEntityManager();
    repository = new JPAQueryFactory(manager)::query;
    OwnedContextResolver resolver =
        type -> {
          return Set.of(new OwnerID(OWNER), new OwnerID("malte"));
        };
    repository = repository.postProcessed(resolver::postProcess);

    manager.getTransaction().begin();
    for (int i = 0; i < 100; i++) {
      manager.persist(MyEntity.random());
    }
    for (int i = 0; i < 100; i++) {
      manager.persist(MyEntity.random(OWNER));
    }
    for (int i = 0; i < 100; i++) {
      manager.persist(MyEntity.random());
    }
    manager.getTransaction().commit();
  }

  @Test
  void test() {
    QMyEntity my = QMyEntity.myEntity;
    repository
        .list(q -> q.select(my).from(my).where(my.sex.eq(MyEntity.Sex.MALE)))
        .forEach(
            e -> {
              System.out.println(e);
              assertEquals(MyEntity.Sex.MALE, e.getSex());
              assertEquals(new OwnerID(OWNER), e.getOwner());
            });
  }
}

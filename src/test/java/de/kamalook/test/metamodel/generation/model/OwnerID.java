package de.kamalook.test.metamodel.generation.model;

import static java.util.Objects.requireNonNull;

import javax.persistence.Basic;
import javax.persistence.Embeddable;

@Embeddable
public class OwnerID {

  @Basic(optional = false)
  private String owner;

  OwnerID() {}

  public OwnerID(String value) {
    this.owner = requireNonNull(value);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    OwnerID other = (OwnerID) obj;
    if (owner == null) {
      if (other.owner != null) {
        return false;
      }
    } else if (!owner.equals(other.owner)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((owner == null) ? 0 : owner.hashCode());
    return result;
  }

  @Override
  public String toString() {
    return owner;
  }
}

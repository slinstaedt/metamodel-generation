package de.kamalook.test.metamodel.generation;

import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.mysema.commons.lang.CloseableIterator;
import com.querydsl.core.Fetchable;
import com.querydsl.core.QueryResults;

@FunctionalInterface
public interface QueryRepository<Q extends Fetchable<?>> {

  class Processed<Q extends Fetchable<?>> implements QueryRepository<Q> {

    private final QueryRepository<Q> delegate;
    private final Consumer<? super Q> before, after;

    public Processed(
        QueryRepository<Q> delegate, Consumer<? super Q> before, Consumer<? super Q> after) {
      this.delegate = requireNonNull(delegate);
      this.before = requireNonNull(before);
      this.after = requireNonNull(after);
    }

    @Override
    public <T> Fetchable<T> apply(QueryBuilder<Q, T> builder) {
      Q query = query();
      before.accept(query);
      Fetchable<T> result = builder.build(query);
      after.accept(query);
      return result;
    }

    @Override
    public Q query() {
      return delegate.query();
    }
  }

  @FunctionalInterface
  interface QueryBuilder<Q, T> {

    Fetchable<T> build(Q query);
  }

  default <T> Fetchable<T> apply(QueryBuilder<Q, T> builder) {
    return builder.build(query());
  }

  default long count(QueryBuilder<Q, ?> builder) {
    return apply(builder).fetchCount();
  }

  default <T> List<T> list(QueryBuilder<Q, T> builder) {
    return apply(builder).fetch();
  }

  default <T> Optional<T> optional(QueryBuilder<Q, T> builder) {
    return Optional.ofNullable(single(builder));
  }

  default QueryRepository<Q> postProcessed(Consumer<? super Q> processor) {
    return new Processed<>(this, Q::getClass, processor);
  }

  default QueryRepository<Q> preProcessed(Consumer<? super Q> processor) {
    return new Processed<>(this, processor, Q::getClass);
  }

  default QueryRepository<Q> processed(Consumer<? super Q> before, Consumer<? super Q> after) {
    return new Processed<>(this, before, after);
  }

  Q query();

  default <T> QueryResults<T> results(QueryBuilder<Q, T> builder) {
    return apply(builder).fetchResults();
  }

  default <T> T single(QueryBuilder<Q, T> builder) {
    return apply(builder).fetchOne();
  }

  default <T> Stream<T> stream(QueryBuilder<Q, T> builder) {
    CloseableIterator<T> iterator = apply(builder).iterate();
    Spliterator<T> spliterator =
        Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED | Spliterator.IMMUTABLE);
    return StreamSupport.stream(spliterator, false).onClose(iterator::close);
  }
}

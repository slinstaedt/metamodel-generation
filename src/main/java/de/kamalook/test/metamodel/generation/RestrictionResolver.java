package de.kamalook.test.metamodel.generation;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import com.querydsl.core.Fetchable;
import com.querydsl.core.JoinExpression;
import com.querydsl.core.QueryMetadata;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.SubQueryExpression;

@FunctionalInterface
public interface RestrictionResolver<T> {

  static Class<?> rawType(AnnotatedElement element) {
    if (element instanceof Class<?>) {
      return (Class<?>) element;
    } else if (element instanceof AnnotatedType) {
      return rawType(((AnnotatedType) element).getType());
    } else if (element instanceof TypeVariable) {
      return rawType((TypeVariable<?>) element);
    } else {
      throw new IllegalArgumentException(element.toString());
    }
  }

  static Class<?> rawType(Type type) {
    if (type instanceof Class<?>) {
      return (Class<?>) type;
    } else if (type instanceof ParameterizedType) {
      return rawType(((ParameterizedType) type).getRawType());
    } else if (type instanceof GenericArrayType) {
      Class<?> componentType = rawType(((GenericArrayType) type).getGenericComponentType());
      return Array.newInstance(componentType, 0).getClass();
    } else if (type instanceof TypeVariable) {
      return rawType((TypeVariable<?>) type);
    } else {
      throw new IllegalArgumentException(type.toString());
    }
  }

  static Class<?> rawType(TypeVariable<?> variable) {
    return Stream.of(variable.getBounds())
        .filter(Class.class::isInstance)
        .map(Class.class::cast)
        .findAny()
        .orElseThrow(() -> new IllegalArgumentException(variable.toString()));
  }

  default <Q extends Fetchable<?> & SubQueryExpression<?>> void postProcess(Q query) {
    QueryMetadata metadata = query.getMetadata();
    metadata
        .getJoins()
        .stream()
        .map(JoinExpression::getTarget)
        .filter(Path.class::isInstance)
        .map(Path.class::cast)
        .map(Path::getRoot)
        .flatMap(
            p -> Stream.of(query.getType().getAnnotatedInterfaces()).map(t -> tryRestrict(t, p)))
        .filter(Objects::nonNull)
        .forEach(metadata::addWhere);
  }

  Optional<Predicate> restrict(AnnotatedType type, Path<? extends T> path);

  @SuppressWarnings("unchecked")
  default Predicate tryRestrict(AnnotatedType type, Path<?> path) {
    boolean assignable =
        rawType(type.getType()).isAssignableFrom(rawType(path.getAnnotatedElement()));
    return assignable ? restrict(type, (Path<? extends T>) path).orElse(null) : null;
  }
}

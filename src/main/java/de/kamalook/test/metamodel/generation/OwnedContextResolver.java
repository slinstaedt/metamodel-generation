package de.kamalook.test.metamodel.generation;

import static de.kamalook.test.metamodel.generation.RestrictionResolver.rawType;

import java.lang.reflect.AnnotatedParameterizedType;
import java.lang.reflect.AnnotatedType;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;

@FunctionalInterface
public interface OwnedContextResolver extends RestrictionResolver<Owned<?>> {

  static Optional<AnnotatedType> resolveOwnedType(AnnotatedType... types) {
    return Stream.of(types)
        .filter(t -> Owned.class.isAssignableFrom(rawType(t.getType())))
        .map(
            t ->
                rawType(t.getType()) == Owned.class
                    ? Optional.of(t)
                    : resolveOwnedType(rawType(t.getType()).getAnnotatedInterfaces()))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .filter(AnnotatedParameterizedType.class::isInstance)
        .map(AnnotatedParameterizedType.class::cast)
        .map(t -> t.getAnnotatedActualTypeArguments()[0])
        .findAny();
  }

  Set<?> resolve(AnnotatedType ownerType);

  @Override
  default Optional<Predicate> restrict(AnnotatedType type, Path<? extends Owned<?>> path) {
    return resolveOwnedType(type).map(this::resolve).map(new QOwned(path).owner::in);
  }
}

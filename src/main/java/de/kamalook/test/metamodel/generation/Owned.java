package de.kamalook.test.metamodel.generation;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public interface Owned<T> {

  T getOwner();
}
